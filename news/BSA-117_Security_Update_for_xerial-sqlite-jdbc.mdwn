[[!meta date="2023-07-17 21:14:45 UTC"]]
	Pierre Gruet uploaded new packages for xerial-sqlite-jdbc which fixed
    the following security problem:
	
	CVE-2023-32697
	
        It was discovered that xerial-sqlite-jdbc had a remote code
        execution vulnerability via JDBC URL, which was caused by
        a predictable UUID choice.
	
	For the bullseye-backports distribution the problem has been fixed in
	version 3.36.0.3+dfsg1-3~bpo11+2.
